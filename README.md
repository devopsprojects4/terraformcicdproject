# TerraformCICDProject


## Project Purpose

The purpose of this project was to utlise the power of GitLab to create a CICD pipeline for Terraform infrastructure code. 

## Project Overview

The CICD pipeline code is stored in the .gitlab-ci.yml file as required by GitHub. This pipeline triggers everytime there is a change committed to the code held in the repository.

This pipeline proved more challenging to implement than the application code pipeline I had created in another project as I had to gain an understanding of how the 'include' templates worked. This functionality allowed me to utlise the 'extends' keywords in the pipeline code to carry out the requied terraform steps (fmt, validate, build etc).

## Terraform State file

GitLab provides the functionality to allow the .tfstate file to be securely stored in the backend and locked. This is important as teams collaborating teams could encounter issues if multiple users are trying to make changes simultaneously to the terraform configuration. 

The project is configured so that a user can create a project access token and utilse a terraform init command locally to initiate the terraform state on their local machine, allowing them to run terraform commands.

## AWS 

To allow the pipeline to connect and make changes to my AWS account, I set up an IAM user in the AWS console and provided it with the relevant access. The access key and secret key are stored in the GitLab variables. These variables are protected and masked and are passed as variables into the provider.tf file. This allows the CICD pipeline to authenticate with AWS and make the relevant configuration changes. 

## Delete infrastructure

The CICD pipeline has a step called 'cleanup' which has been developed to destroy all resources in AWS when a commit is made with the message "destroy".

