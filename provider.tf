terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "http" {
    address = "https://gitlab.com/api/v4/projects/46710748/terraform/state/default"
    lock_address = "https://gitlab.com/api/v4/projects/46710748/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/46710748/terraform/state/default/lock"
  }
}

provider "aws" {
  region = "eu-west-2"
}