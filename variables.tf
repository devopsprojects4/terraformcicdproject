variable "ami" {
  type        = string
  description = "Amazon Linux 2023 AMI ID in eu-west-2 Region"
  default     = "ami-02fb3e77af3bea031"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}